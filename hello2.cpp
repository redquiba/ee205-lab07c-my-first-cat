//////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Ryan Edquiba <redquiba@hawaii.edu>
/// @date   3/3/22
//////////////////////////////////////////////////////////////////////////////

#include <iostream>

int main() {

   std::cout << "Hello World" << std::endl ;

   return 0;
}
