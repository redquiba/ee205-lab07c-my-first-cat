//////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 07c - My First Cat - EE 205 - Spr 2022
///
/// @file hello2.cpp
/// @version 1.0
///
/// @author Ryan Edquiba <redquiba@hawaii.edu>
/// @date   3/3/22
//////////////////////////////////////////////////////////////////////////////

#include <iostream>

using namespace std;

class Cat {
   public:
      void sayHello() {
         cout << "Meow Meow" << endl;
      }
} ;


int main() {

   Cat myCat;
   myCat.sayHello();

   return 0;
}

